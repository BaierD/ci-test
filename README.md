# SoSy-Lab CI Test and Info Project

This project is intended for testing our CI config for GitLab
and gathering information about the various possible scenarios:

1. pushes to main
2. pushes to another branch in this project (no MR open)
3. pushes to another branch in this project with an open MR
4. pushes to main in a fork
5. pushes to another branch in a fork (no MR open)
6. pushes to another branch in a fork with an open MR against this project
7. pipelines triggered manually for an MR from a fork

There are even more possible scenarios (tags, schedules, manual triggers),
but these generally behave like 1. and are not explicitly tested.

Each commit here is supposed to add a file to `commits/` with a descriptive name
that starts with the number of the scenario above,
such that we can see whether `git diff` works as desired.
The CI jobs show all the information available from GitLab
as well as information about the state of the git checkout.

This project is configured with [merged results pipelines](https://docs.gitlab.com/ee/ci/pipelines/merged_results_pipelines.html)
and the CI config enables [merge request pipelines](https://docs.gitlab.com/ee/ci/pipelines/merge_request_pipelines.html).
Redundant push/MR pipelines are [disabled](https://docs.gitlab.com/ee/ci/jobs/job_control.html#avoid-duplicate-pipelines).

## Findings

- MRs created from a fork, but by project members behave like internal MRs.
  Their pipelines immediately run in the context of the parent project, not the fork.
  An example for this is !2, where the [pipelines](https://gitlab.com/sosy-lab/ci-test/-/merge_requests/2/pipelines)
  look like the [pipelines](https://gitlab.com/sosy-lab/ci-test/-/merge_requests/1/pipelines) from !1.
- MRs create from a fork by non-members behave differently.
  Pipelines for such MRs are executed in the context of the fork,
  and do not use the "merged results" feature,
  as can be seen for the [pipelines](https://gitlab.com/sosy-lab/ci-test/-/merge_requests/3/pipelines) for !3.
  Only once a project member triggers a pipeline using the "Run pipeline" button
  a pipeline in the context of the parent project and with "merged results" is created.
- The variable `CI_MERGE_REQUEST_DIFF_BASE_SHA` is present in all MR pipelines
  (no matter whether from a fork or not and whether "merged results" is active)
  and designates the appropriate base for computing the diff of the MR
  (what git calls the "merge base").
  So to compute the same diff as GitLab shows in the UI,
  use `git diff $CI_MERGE_REQUEST_DIFF_BASE_SHA...`.
